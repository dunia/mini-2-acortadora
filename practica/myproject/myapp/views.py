from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .models import Contenido
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.template import loader
from django.urls import reverse
# Create your views here.
contador_global = 0

@csrf_exempt
def formato(url):
    if not(url.startswith("http://") or url.startswith("https://")):
        url = f"http://localhost:1234/{url}"
    return url


@csrf_exempt
def esta_vacio(short):
    return short == ""


@csrf_exempt
def get_content(request, corta):
    global contador_global
    print('eooooo')
    if request.method == 'POST':
        global contador_global
        print('holaaa')
        corta = request.POST.get['short', '']
        valor = request.POST['url']
        valor = valor.formato()
        if not corta:
            contador_global += 1
            corta = str(contador_global)
        else:
            try:
                corta_numero = int(corta)
                if corta_numero > contador_global:
                    contador_global = corta_numero
            except ValueError:
                pass

        print(corta)
        c = Contenido(url=valor, short=corta)
        print('ariadna')
        c.save()
        print('kokokok')
        content_list = Contenido.objects.all()
        template = loader.get_template('app/formulario.html')
        context = {'content_list': content_list}
        return HttpResponse(template.render(context, request))

    try:
        #se hace un get donde se pide /algo , ese algo es una url corta la cual tengo que redirigir
        peticion_url_corta = Contenido.objects.get(short=corta)

        respuesta = peticion_url_corta.url
        return HttpResponseRedirect(respuesta)
    except Contenido.DoesNotExist:
        content_list = Contenido.objects.all()
        template = loader.get_template('app/formulario.html')
        context = {'content_list': content_list}
        return HttpResponse(template.render(context, request))

@csrf_exempt# caso GET donde solo se pida la barra pues imprimos lista
def index(request):
    global contador_global  # Accedemos a la variable global

    if request.method == 'POST':
        corta = request.POST.get('short', '')  # Usamos get para obtener el valor con un valor por defecto de ''
        valor = request.POST['url']
        valor = formato(valor)

        if not corta:  # Si corta está vacío
            contador_global += 1  # Incrementamos el contador global
            corta = str(contador_global)  # Asignamos el valor del contador como corta
        else:
            try:
                corta_numero = int(corta)
                if corta_numero > contador_global:
                    contador_global = corta_numero
            except ValueError:
                pass

        c = Contenido(url=valor, short=corta)
        c.save()

    content_list = Contenido.objects.all()
    template = loader.get_template('app/formulario.html')
    context = {'content_list': content_list}
    return HttpResponse(template.render(context, request))















