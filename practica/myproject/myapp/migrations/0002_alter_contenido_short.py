# Generated by Django 5.0.3 on 2024-04-06 12:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("myapp", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="contenido",
            name="short",
            field=models.CharField(blank=True, max_length=64),
        ),
    ]
