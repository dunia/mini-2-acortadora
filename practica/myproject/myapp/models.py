from django.db import models

class Contenido(models.Model):
    url = models.TextField(max_length=64)
    short = models.CharField(max_length=64, blank=True)


